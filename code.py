#!/usr/bin/python

# Science Fair - by Jeffrey
#
# Adafruit UV sensor library ported to Python by Joe Gutting
# With use of Adafruit SI1145 library for Arduino, Adafruit_GPIO.I2C & BMP Library by Tony DiCola
#
# Notice: The license below legally permits me to use parts of the library for my needs.
# The license below is unchanged from the original:
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

# Can enable debug output by uncommenting:
#import logging
#logging.basicConfig(level=logging.DEBUG)

import time # For slowing down the while loop
import SI1145.SI1145 as SI1145 # For interfacing with the Adafruit sensor
import requests # For communicating with the Philips Hue bulb
import sys # Quit program once trials are over

sensor = SI1145.SI1145()

print 'Press Cntrl + Z to cancel'

hue = 0 # Tracking the hue value displayed on the bulb
x = 1 # Track trials
while True:
        print "Trial:           " + str(x) + "/5"
        # Set lightbulb hue
        r = requests.put("http://192.168.1.211/api/[API_KEY]/lights/4/state", data = '{"on":true, "sat":254, "bri":255,"hue": ' + str(hue) + '}')
        # Wait two seconds before recording data
        time.sleep(2)
        # Get sensor data
        vis = sensor.readVisible()
        IR = sensor.readIR()
        UV = sensor.readUV()
        uvIndex = UV / 100.0
        # Print gathered data
        print "Visible Light:   " + str(vis)
        print "Infrared Light:  " + str(IR)
        print "UV Index:        " + str(uvIndex)
        print "Lightbulb Hue:   " + str(hue)
        print ''
        # Record data to CSV
        record = open('dataScienceFair.csv', 'a')
        record.write(str(hue) + "," + str(vis) + "," + str(IR) + "," + str(uvIndex) + "," + "\n")
        record.close()
        # Increment hue
        hue += 650
        if hue >= 65000:
            hue = 0
            x = x + 1
            if x > 5:
                print "Trials complete. Quitting..."
                sys.exit(0)
        # Wait a few seconds and repeat.
        time.sleep(3)